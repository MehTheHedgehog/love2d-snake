# Snake in Love2D #

Reimplementation of popular snake game in lua (with Love2D framework)

## How to play ##
### Requirements ###
* [Love2d](https://love2d.org/)

Download newest snake.love from [release page](https://gitlab.com/MehTheHedgehog/love2d-snake/-/releases).

Download Love2D from [Love2D site](https://love2d.org/).
If you have already installed Love2D just run it and drag&drop snake file into Love window. 
If you have associated .love files, just double click on snake.love

## Controls ##

* [Up, Down, Left, Right] - change direction of snake
* Esc - Exit game

## Screenshots ##
![Snake](https://s3.mehthehedgehog.be/assets/gitlab/love2d-snake/snake-play.gif)