#!/bin/bash

NAME=$1
LINK=$2

ASSETS=$(curl --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
     "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links" 2>/dev/null)

ASSET_ID=$(echo ${ASSETS} | jq -r '.[] | select((.name=="'${NAME}'") or (.url=="'${LINK}'")) | .id')

if [ -z "${ASSET_ID}" ]
then
    echo "Creating link"
    curl --request POST \
        --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
        --data name="${NAME}" \
        --data url="${LINK}" \
        "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links"
else
    echo "Updating link"
    curl --request PUT \
        --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
        --data name="${NAME}" \
        --data url="${LINK}" \
        "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links/${ASSET_ID}"
fi


