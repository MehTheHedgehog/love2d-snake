local Game = {}
Game.player = {}
Game.move_inter = 0
Game.foods = {}
Game.game_over = false

function love.load()
    Game.map_width = 50
    Game.map_height = 50
    Game.cell_width = (love.graphics.getWidth() / Game.map_width)
    Game.cell_height = (love.graphics.getHeight() / Game.map_height)

    Game.player.head_position = { x = 80, y = 30, direct = 'up' }
    Game.player.cells = {Game.player.head_position}
    Game.player.velocity = 0.05
    Game.player.initial_length = 20

    for i = 1, Game.player.initial_length do
        local cell = { x = Game.player.head_position.x,
                       y = Game.player.head_position.y + i,
                       direct = Game.player.head_position.direct
                     }
        table.insert(Game.player.cells, cell)
    end

    table.insert(Game.foods, { x = love.math.random(0, Game.map_width), y = love.math.random(0, Game.map_height) })
end

function love.update(dt)
    if Game.game_over then
        return
    end

    if Game.move_inter > Game.player.velocity then
        for i = #Game.player.cells, 1, -1 do
            local cell = Game.player.cells[i]

            if i == #Game.player.cells
                and cell.x == Game.foods[1].x
                and cell.y == Game.foods[1].y
            then
                table.insert(Game.player.cells, { x = cell.x, y = cell.y, direct = cell.direct})
                table.remove(Game.foods, 1)
            end

            if cell.direct == 'up' then
                cell.y = (cell.y - 1)
            elseif cell.direct == 'down' then
                cell.y = (cell.y + 1)
            elseif cell.direct == 'left' then
                cell.x = (cell.x - 1)
            elseif cell.direct == 'right' then
                cell.x = (cell.x + 1)
            end

            if cell.x >= Game.map_width then
                cell.x = 0
            end
            if cell.x < 0 then
                cell.x = Game.map_width - 1
            end
            if cell.y >= Game.map_height then
                cell.y = 0
            end
            if cell.y < 0 then
                cell.y = Game.map_width - 1
            end

            if i == 1
                and Game.player.head_position.x == Game.foods[#Game.foods].x
                and Game.player.head_position.y == Game.foods[#Game.foods].y
            then
                table.insert(Game.foods, { x = love.math.random(0, Game.map_width - 1), y = love.math.random(0, Game.map_height - 1) })
            end

            if i > 1 then
                cell.direct = Game.player.cells[i - 1].direct
            end
        end

        for cells_index = 2, #Game.player.cells do
            local cell = Game.player.cells[cells_index]
            if Game.player.head_position.x == cell.x
                and Game.player.head_position.y == cell.y
            then
                Game.game_over = true
            end
        end

        Game.move_inter = ((Game.move_inter - Game.player.velocity) + dt)
    else
        Game.move_inter = (Game.move_inter + dt)
    end
end

function love.keypressed(key, scancode, isrepeat)
    if key == 'escape' then
        love.event.quit()
    end

    if key == 'up'
        and Game.player.head_position.direct ~= 'down'
    then
        Game.player.head_position.direct = 'up'
    elseif key == 'down' 
        and Game.player.head_position.direct ~= 'up'
    then
        Game.player.head_position.direct = 'down'
    elseif key == 'left'
        and Game.player.head_position.direct ~= 'right'
    then
        Game.player.head_position.direct = 'left'
    elseif key == 'right'
        and Game.player.head_position.direct ~= 'left'
    then
        Game.player.head_position.direct = 'right'
    end
end

function love.draw()
    love.graphics.setColor(1, 1, 1, 1)

    for _, cell in ipairs(Game.player.cells) do
        love.graphics.rectangle('fill', (Game.cell_width * cell.x), (Game.cell_height * cell.y), Game.cell_width, Game.cell_height)
    end

    for _, food in ipairs(Game.foods) do
        love.graphics.rectangle('fill', (Game.cell_width * food.x), (Game.cell_height * food.y), Game.cell_width, Game.cell_height)
    end

    if Game.game_over then
        love.graphics.print("Game Over!!!", love.graphics.getWidth() / 2, love.graphics.getHeight() / 2, 0 )
    end

    love.graphics.print("Score: " .. ((#Game.player.cells) - (Game.player.initial_length + 1) + (#Game.foods - 1)), 10, 10, 0 )

end